package com.misitio.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.misitio.connection.SQLServerConnection;
import com.misitio.model.Usuario;

public class UsuarioDB {

		private SQLServerConnection sqlServerConnection = new SQLServerConnection();
		public String crearUsuario(Usuario usuario) {
			String mensaje = "";
			try {
				Connection connection = sqlServerConnection.getConnection();
				String sqlInsert = "INSERT INTO USUARIOS (nombre, apellidoPaterno, apellidoMaterno, email, telefono, pais, password, activo) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
				PreparedStatement statement = connection.prepareStatement(sqlInsert);
				statement.setString(1, usuario.getNombre());
				statement.setString(2, usuario.getApellidoMaterno());
				statement.setString(3, usuario.getApellidoMaterno());
				statement.setString(4, usuario.getEmail());
				statement.setString(5, usuario.getTelefono());
				statement.setString(6, usuario.getPais());
				statement.setString(7, usuario.getPassword());
				statement.setBoolean(8, false);
				statement.executeUpdate();
				mensaje += "Usuaario creado de forma exitosa, se Activa via Email";
			} catch (SQLException e) {
				mensaje += e.getMessage();
				e.printStackTrace();
			}
			return mensaje;
			
		}
}
